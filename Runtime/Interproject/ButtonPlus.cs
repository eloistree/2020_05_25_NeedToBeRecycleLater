﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonPlus : MonoBehaviour ,IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent m_onDown;
    public UnityEvent m_onUp;

    public void OnPointerDown(PointerEventData eventData)
    {
        m_onDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        m_onUp.Invoke();
    }
    
}
